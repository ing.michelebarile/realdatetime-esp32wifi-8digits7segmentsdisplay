# Realdatetime Esp32wifi 8digits7segmentsdisplay

The script provides the real date and time thanks to NTP server connection via Wifi. Then it sends data to 8 digit 7 segments display.

![drawing](img/1.jpeg)


## Getting started

To make it easy for you to get started with this script, here's a list of recommended next steps.

- Install Arduino IDE
- Plug in ESP32
- Check into device manager if driver are all be installed; instead open /driver esp32 directory and launch **CP210xVCPInstaller_[x64|x86].exe**

- Open Arduino IDE

- Go to File -> Preferences and add the following json into Additional Boards Manager URLs:

    ![drawing](img/preferences%20json.png)

- Go to Tool -> Board -> Board Manager and select as the following screenshot

    ![drawing](img/board%20manager.png)

- Select the following Board ad screenshot

    ![drwaing](img/board%20model.png)

N.B.: Scketch uploading needs to have a Capacitor of 10μF with the positive pin on 23 (Esp32) and negative to GND.

## ESP32Wifi PinOut

![drawing](img/ESP32-Pinout.jpg)

## How it works

- Set your SSID and WIFI PASSWORD into code

- Check the correct pins connections as following photo:

    ![drawing](img/3.jpeg)

- Enjoy!

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## External links

[ESP32 NTP Client-Server: Get Date and Time (Arduino IDE)](https://randomnerdtutorials.com/esp32-date-time-ntp-client-server-arduino/)
